using System;
using System.Collections.Generic;

namespace Training.DomainClasses
{
    public class Pet : IEquatable<Pet>
    {
        public bool Equals(Pet other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return String.Equals(name, other.name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Pet) obj);
        }

        public override int GetHashCode()
        {
            return (name != null ? name.GetHashCode() : 0);
        }

        public static bool operator ==(Pet left, Pet right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Pet left, Pet right)
        {
            return !Equals(left, right);
        }

        public Sex sex;
        public string name { get; set; }
        public int yearOfBirth { get; set; }
        public float price { get; set; }
        public Race race { get; set; }
        
        public static Criteria<Pet> IsMouse()
        {
            return new IsAnyOfRaceCriteria(Race.Mouse); ;
        }
        
        public static Criteria<Pet> IsBornAfter(int year)
        {
            return new IsBornAfterCriteria(year);
        }
        
        public class IsAnyOfRaceCriteria : Criteria<Pet>
        {
            private readonly List<Race> _allowedRaces;

            public IsAnyOfRaceCriteria(params Race[] allowedRaces)
            {
                _allowedRaces = new List<Race>(allowedRaces);
            }

            public bool IsSatisfiedBy(Pet item)
            {
                return _allowedRaces.Contains(item.race);
            }
        }

        public class IsBornAfterCriteria : Criteria<Pet>
        {
            private readonly int _year;

            public IsBornAfterCriteria(int year)
            {
                _year = year;
            }

            public bool IsSatisfiedBy(Pet item)
            {
                return item.yearOfBirth > _year;
            }
        }

        public class IsSexCriteria : Criteria<Pet>
        {
            private readonly Sex _sex;

            public IsSexCriteria(Sex sex)
            {
                _sex = sex;
            }

            public bool IsSatisfiedBy(Pet item)
            {
                return item.sex == _sex;
            }
        }

        public static Criteria<Pet> IsDog()
        {
            return new IsAnyOfRaceCriteria(Race.Dog);
        }

        public static Criteria<Pet> IsRabbit()
        {
            return new IsAnyOfRaceCriteria(Race.Rabbit);
        }

        public static Criteria<Pet> IsMale()
        {
            return new IsSexCriteria(Sex.Male);
        }
    }
}