using System;
using System.Collections.Generic;

namespace Training.DomainClasses
{
    public static class FilteringEntryPointExtension
    {
        public static Criteria<TItem> EualsTo<TItem, TResult>(this FilteringEntryPoint<TItem, TResult> filteringEntryPoint, TResult fieldValue)
        {
            Criteria<TItem> resultCriteria = new AnonymousCriteria<TItem>(pet => filteringEntryPoint.fieldExtractor(pet).Equals(fieldValue));
            return filteringEntryPoint.ApplyNegation(resultCriteria);
        }

        public static Criteria<TItem> GreaterThan<TItem, TResult>(this FilteringEntryPoint<TItem, TResult> filteringEntryPoint, TResult fieldValue) where TResult : IComparable<TResult>
        {
            Criteria<TItem> resultCriteria = new AnonymousCriteria<TItem>(item => fieldValue.CompareTo(filteringEntryPoint.fieldExtractor(item)) < 0);
            return filteringEntryPoint.ApplyNegation(resultCriteria);
        }

        public static Criteria<TItem> EualsToAny<TItem, TResult>(this FilteringEntryPoint<TItem, TResult> filteringEntryPoint, params TResult[] fieldValues)
        {
            var allowedValues = new List<TResult>(fieldValues);
            Criteria<TItem> resultCriteria = new AnonymousCriteria<TItem>(item => allowedValues.Contains(filteringEntryPoint.fieldExtractor(item)));
            return filteringEntryPoint.ApplyNegation(resultCriteria);
        }
    }
}