using Training.DomainClasses;

public static class CriteriaBuilderExtensions
{
    public static Criteria<TItem> EualsTo<TItem, TResult>(this CriteriaBuilder<TItem, TResult> criteriaBuilder, TResult fieldValue)
    {
        return new AnonymousCriteria<TItem>(item => criteriaBuilder._fieldExtractor(item).Equals(fieldValue));
    }
}