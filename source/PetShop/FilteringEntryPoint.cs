using System;

namespace Training.DomainClasses
{
    public class FilteringEntryPoint<TItem, TResult> 
    {
        public readonly Func<TItem, TResult> fieldExtractor;
        public readonly bool not ;

        public FilteringEntryPoint(Func<TItem, TResult> fieldExtractor) : this(fieldExtractor, false)
        {
        }

        private FilteringEntryPoint(Func<TItem, TResult> fieldExtractor, bool not )
        {
            this.not = not;
            this.fieldExtractor = fieldExtractor;
        }
        public FilteringEntryPoint<TItem, TResult> Not()
        {
            return new FilteringEntryPoint<TItem, TResult>(fieldExtractor,!not);
        }

        public Criteria<TItem> ApplyNegation(Criteria<TItem> resultCriteria)
        {
            if (not)
                resultCriteria = new Negation<TItem>(resultCriteria);
            return resultCriteria;
        }
    }
}