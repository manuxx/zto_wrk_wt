namespace Training.DomainClasses
{
    public class Negation<TItem> : Criteria<TItem>
    {
        private readonly Criteria<TItem> _criteriaToNagate;

        public Negation(Criteria<TItem> criteriaToNagate)
        {
            _criteriaToNagate = criteriaToNagate;
        }

        public bool IsSatisfiedBy(TItem item)
        {
            return !_criteriaToNagate.IsSatisfiedBy(item);
        }
    }
}