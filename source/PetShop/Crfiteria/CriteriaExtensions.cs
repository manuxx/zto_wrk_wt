using Training.DomainClasses;

public static class CriteriaExtensions
{
    public static AndCriteria<TItem> And<TItem>(this Criteria<TItem> leftCriteria, Criteria<TItem> rightCriteria)
    {
        return new AndCriteria<TItem>(leftCriteria, rightCriteria);
    }

    public static OrCriteria<TItem> Or<TItem>(this Criteria<TItem> leftCriteria, Criteria<TItem> rightCriteria)
    {
        return new OrCriteria<TItem>(leftCriteria, rightCriteria);
    }
}