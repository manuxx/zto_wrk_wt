﻿using System;


namespace Training.DomainClasses
{
    public class Where<TSource>
    {
        public static FilteringEntryPoint<TSource, TResult> hasAn<TResult>(Func<TSource, TResult> fieldExtractor) 
        {
            return new FilteringEntryPoint<TSource, TResult>(fieldExtractor);
        }
    }
}