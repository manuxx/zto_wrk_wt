using System;
using System.Collections.Generic;
using Training.DomainClasses;

public static class EnumerableExtensions
{
    public static IEnumerable<TItem> OneAtATime<TItem>(this IEnumerable<TItem> items)
    {
        foreach (var item in items)
        {
            yield return item;
        }
    }
    public static IEnumerable<Pet> ThatSatisfy(this IEnumerable<Pet> items, Criteria<Pet> condition)
    {
        foreach (var item in items)
        {
            if (condition.IsSatisfiedBy(item))
            {
                yield return item;
            }
        }
    }
    public static IEnumerable<Pet> ThatSatisfy(this IEnumerable<Pet> items, Predicate<Pet> condition)
    {
        return items.ThatSatisfy(new AnonymousCriteria<Pet>(condition));
    }
}