using System;
using System.Collections.Generic;
//using System.Linq;

namespace Training.DomainClasses
{
    public class PetShop
    {
        private readonly IList<Pet> _petsInTheStore;

        public PetShop(IList<Pet> petsInTheStore)
        {
            this._petsInTheStore = petsInTheStore;
        }

        public IEnumerable<Pet> AllPets()
        {
            return new ReadOnlySet<Pet>(_petsInTheStore);
        }

        public void Add(Pet newPet)
        {
            if (_petsInTheStore.Contains(newPet))
                    return;

            _petsInTheStore.Add(newPet);
        }

        public IEnumerable<Pet> AllPetsSortedByName()
        {
            var result = new List<Pet>(_petsInTheStore);
            result.Sort((x, y) => x.name.CompareTo( y.name));
            return result;
        }

        public IEnumerable<Pet> AllPetsButNotMice()
        {
            return _petsInTheStore.ThatSatisfy(new Negation<Pet>(Pet.IsMouse()));
        }


        public IEnumerable<Pet> AllDogsBornAfter2010() 
        {
            return _petsInTheStore.ThatSatisfy(Pet.IsDog().And(Pet.IsBornAfter(2010)));
        }

        public IEnumerable<Pet> AllMaleDogs()
        {
            return _petsInTheStore.ThatSatisfy(Pet.IsDog().And(Pet.IsMale()));
        }

        public IEnumerable<Pet> AllPetsBornAfter2011OrRabbits()
        {
            return _petsInTheStore.ThatSatisfy(Pet.IsBornAfter(2011).Or(Pet.IsRabbit()));
        }
    }
}